#ifndef STREAM9_XDG_APPLICATION_MIME_APPS_LIST_HPP
#define STREAM9_XDG_APPLICATION_MIME_APPS_LIST_HPP

#include <stream9/xdg/error.hpp>

#include "id_set.hpp"
#include "time_stamp.hpp"

#include "../namespace.hpp"

#include <stream9/array.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/function_ref.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>
#include <stream9/unique_table.hpp>

namespace stream9::xdg::applications {

class mime_apps_list
{
public:
    using entry_map = unique_table<string /*mime_type*/, id_set >;

public:
    mime_apps_list(string_view path) noexcept;

    // accessor
    cstring_view path() const noexcept { return m_path; }

    // query
    id_set const&
        added_associations(string_view mime_type) const;

    id_set const&
        removed_associations(string_view mime_type) const;

    id_set const&
        default_applications(string_view mime_type) const;

    void associated_mime_types(string_view prefix,
             function_ref<void(string_view)>) const;

    bool empty() const noexcept;

    // modifier
    void add_association(string_view mime_type, string_view id);

    void remove_association(string_view mime_type, string_view id);

    void add_default_application(string_view mime_type, string_view id);

    void remove_default_application(string_view mime_type, string_view id);

    void reload();

    void save();

private:
    friend std::ostream& operator<<(std::ostream&, mime_apps_list const&);

    string m_path;
    opt<time_stamp> m_mtime;
    bool m_changed = false;

    entry_map m_added_assocs;
    entry_map m_removed_assocs;
    entry_map m_default_apps;
};

} // namespace stream9::xdg::applications

#endif // STREAM9_XDG_APPLICATION_MIME_APPS_LIST_HPP
