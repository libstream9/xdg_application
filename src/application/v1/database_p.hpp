#ifndef STREAM9_XDG_APPLICATION_APPLICATION_DATABASE_P_HPP
#define STREAM9_XDG_APPLICATION_APPLICATION_DATABASE_P_HPP

#include <stream9/xdg/application/v1/database.hpp>

#include "directory.hpp"

#include <stream9/array.hpp>
#include <stream9/node.hpp>
#include <stream9/optional.hpp>
#include <stream9/string_view.hpp>
#include <stream9/unique_table.hpp>
#include <stream9/xdg/mime/database.hpp>

namespace stream9::xdg::applications::v1 {

using directory_set = array<directory>;

class database::impl
{
public:
    impl();
    impl(mime::database const&);

    void init_directories();

    preference_ordered_desktop_entry_set
        find_applications(string_view mime_type) const;

    opt<desktop_entry>
        find_default_application(string_view mime_type) const;

    opt<desktop_entry>
        find_default_application(string_view mime_type,
                                 database::error_set&) const;

    desktop_entry_set all_applications() const;

    unique_array<string_view>
        associated_mime_types(string_view prefix) const;

    array<string_view>
        data_directory_paths() const;

    opt<desktop_entry>
        load_desktop_entry(string_view id) const noexcept;

    auto& directories() noexcept { return m_directories; }

    auto& config_home() noexcept { return m_directories[0]; }

    auto& entries() noexcept { return m_entries; }

    void delete_directory(directory&) noexcept;

    opt<directory&> scan_directory_for_desktop_entry(string_view/*id*/) noexcept;
    opt<string> scan_path_for_desktop_entry(string_view/*id*/);

private:
    mime::database const& mime_db() const noexcept;

private:
    directory_set m_directories;
    mime::database const* m_external_mime_db = nullptr;
    node<mime::database> m_internal_mime_db;
    unique_table<string/*id*/, string/*dirpath*/> m_entries;
};

} // namespace stream9::xdg::applications::v1

#endif // STREAM9_XDG_APPLICATION_APPLICATION_DATABASE_P_HPP
