#ifndef STREAM9_DESKTOP_PARSER_DESKTOP_ENTRY_KEY_HPP
#define STREAM9_DESKTOP_PARSER_DESKTOP_ENTRY_KEY_HPP

#include "../parser.hpp"

#include <stream9/xdg/error.hpp>
#include <stream9/xdg/desktop_entry/locale.hpp>

#include <stream9/array.hpp>
#include <stream9/string_view.hpp>

namespace stream9::xdg::parser::desktop_entry_key {

struct result {
    string_view name;
    class desktop_entry_::locale locale;
};

result parse(string_view text,
             location_tracker const* locs = nullptr,
             array<error>* errors = nullptr);

} // namespace stream9::xdg::parser::desktop_entry_key

#endif // STREAM9_DESKTOP_PARSER_DESKTOP_ENTRY_KEY_HPP
