#include "mime_info_cache.hpp"

#include <stream9/xdg/environment.hpp>
#include <stream9/xdg/error.hpp>

#include "namespace.hpp"
#include "parser/mime_info_cache.hpp"

#include <stream9/log.hpp>
#include <stream9/strings/split.hpp>

namespace stream9::xdg::applications {

mime_info_cache::
mime_info_cache(string_view path) noexcept
    try : m_path { path }
{
    reload();
}
catch (...) {
    print_error(log::warn(), {
        { "path", path }
    });
}

void mime_info_cache::
reload()
{
    try {
        auto o_st = env().file_status(m_path);
        if (o_st) {
            auto& mtime = o_st->st_mtim;
            if (m_mtime) {
                if (mtime == *m_mtime) return;
            }

            m_text = env().load_file(m_path);
            m_mtime = mtime;

            m_entries.clear();
            array<error> errors;

            parser::mime_info_cache::parse(
                m_text,
                m_entries,
                &errors
            );

            if (!errors.empty()) {
                log::warn() << "parse error on" << m_path;

                for (auto const& e: errors) {
                    log::warn() << e;
                }
            }
        }
        else {
            m_entries.clear();
            m_text.clear();
            m_mtime = null;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

mime_info_cache::desktop_file_id_list mime_info_cache::
find(string_view mime_type) const
{
    try {
        desktop_file_id_list result;

        auto it = m_entries.find(mime_type);
        if (it != m_entries.end()) {
            auto const& ids = it->value;

            result = str::split(ids, ";");
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::applications
