#ifndef STREAM9_DESKTOP_PARSER_DESKTOP_ENTRY_HPP
#define STREAM9_DESKTOP_PARSER_DESKTOP_ENTRY_HPP

#include "../parser.hpp"

#include <stream9/xdg/namespace.hpp>
#include <stream9/xdg/desktop_entry/entry.hpp>

#include <stream9/array.hpp>
#include <stream9/string_view.hpp>

#include <system_error>

namespace stream9::xdg::parser::desktop_entry {

void parse(string_view text,
           array<desktop_entry_::entry_group, 1>& groups,
           array<error>* const errors = nullptr);

enum class errc {
    unknown_key,
    duplicated_key,
    duplicated_group,
    missing_required_group,
    missing_required_entry,
    missing_registered_action_group,
    localized_key_on_string_entry,
    localized_key_on_boolean_entry,
    unknown_desktop_entry_type,
    unknown_desktop_entry_key,
    invalid_value,
    invalid_boolean_value,
    duplicated_action_key,
    unregistered_action_group,
    entry_type_mismatch,
    localized_entry_without_default,
};

std::error_category& error_category();

inline std::error_code
make_error_code(errc const e)
{
    return { static_cast<int>(e), error_category() };
}

} // namespace stream9::xdg::parser::desktop_entry

namespace std {

template<>
struct is_error_code_enum<stream9::xdg::parser::desktop_entry::errc>
    : std::true_type
{};

} // namespace std

#endif // STREAM9_DESKTOP_PARSER_DESKTOP_ENTRY_HPP
