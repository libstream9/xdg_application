#ifndef STREAM9_DESKTOP_SCANNER_EXEC_STRING_HPP
#define STREAM9_DESKTOP_SCANNER_EXEC_STRING_HPP

#include "../namespace.hpp"
#include "../scanner.hpp"

#include <stream9/string_view.hpp>

#include <cassert>
#include <system_error>

namespace stream9::xdg::scanner::exec_string {

// Grammar
//
// exec := empty | argument *(+space argument)
// argument := quoted_string | unquoted_string
// quoted_string := '"' escaped_string '"'
// unquoted_string := *(string_char - reserved_char)
// escaped_string := *(escaped_char | (string_char - escapee_char))
// escaped_char := '\' escapee_char
// reserved_char := ' ' | '\t' | '\n' | '"' | "'" | '\' |
//                  '>' | '<' | '~' | '|' | '&' | ';' |
//                  '$' | '*' | '?' | '#' | '(' | ')' | '`'
// escapee_char := '"' | '`' | '$' | '\'
// string_char := <any ASCII char> - <ASCII control char except CR LF TAB>
// space := ' ' | '\t'
//

using iterator = string_view::iterator;

template<typename T>
concept content_handler =
    requires (T&& t, next_action a, std::error_code e, iterator i) {
        t.begin_argument(a);
        t.end_argument(a);
        t.on_character('c', a);
        t.on_error(e, i, a);
    };

template<content_handler Handler>
bool scan(string_view text, Handler&&);

enum class errc {
    ok = 0,
    unclosed_quote,
    unquoted_reserved_char,
    unescaped_char,
    unknown_escape_sequence,
};

//
// implementation
//
template<content_handler T>
class context
{
public:
    context(T& h) : m_handler { h } {}

    void emit_begin_argument()
    {
        next_action a = next_action::proceed;

        m_handler.begin_argument(a);

        execute_action(a);
    }

    void emit_end_argument()
    {
        next_action a = next_action::proceed;

        m_handler.end_argument(a);

        execute_action(a);
    }

    void emit_character(char const c)
    {
        next_action a = next_action::proceed;

        m_handler.on_character(c, a);

        execute_action(a);
    }

    void emit_error(std::error_code const& ec, iterator const it)
    {
        next_action a = next_action::stop;

        m_handler.on_error(ec, it, a);

        execute_action(a);
    }

private:
    T& m_handler;
};

inline bool
is_space(char const c)
{
    return c == ' ' || c == '\t';
}

inline bool
is_escapee_char(char const c)
{
    switch (c) {
        case '"': case '`': case '$': case '\\':
            return true;
        default:
            return false;
    }
}

inline bool
is_reserved_char(char const c)
{
    switch (c) {
        case ' ': case '\t': case '\n': case '"': case '\'':
        case '\\': case '>': case '<': case '~': case '|':
        case '&': case ';': case '$': case '*': case '?':
        case '#': case '(': case ')': case '`':
            return true;
        default:
            return false;
    }
}

template<content_handler T>
bool
escaped_char(iterator& it, iterator const end, context<T>& cxt)
{
    assert(it != end);

    if (*it == '\\') {
        ++it;
        if (it == end) {
            cxt.emit_character('\\');
            return true;
        }
        else if (is_escapee_char(*it)) {
            cxt.emit_character(*it);
            ++it;
            return true;
        }
        else {
            cxt.emit_error(errc::unknown_escape_sequence, it - 1);
        }
    }

    return false;
}

template<content_handler T>
void
unescaped_char(iterator& it, iterator const end, context<T>& cxt)
{
    assert(it != end);

    if (is_escapee_char(*it)) {
        cxt.emit_error(errc::unescaped_char, it);
    }

    cxt.emit_character(*it);
    ++it;
}

template<content_handler T>
void
unquoted_string(iterator& it, iterator const end, context<T>& cxt)
{
    assert(it != end);
    assert(!is_space(*it));

    for (; it != end; ++it) {
        if (is_space(*it)) break;

        cxt.emit_character(*it);
    }
}

template<content_handler T>
void
quoted_string(iterator& it, iterator const end, context<T>& cxt)
{
    assert(it != end);
    assert(*it == '"');

    ++it; // skip '"'

    while (true) {
        if (it == end) {
            cxt.emit_error(errc::unclosed_quote, it);
        }
        if (*it == '"') {
            ++it;
            break;
        }

        if (!escaped_char(it, end, cxt)) {
            unescaped_char(it, end, cxt);
        }
    }
}

template<content_handler T>
void
argument(iterator& it, iterator const end, context<T>& cxt)
{
    assert(it != end);

    cxt.emit_begin_argument();

    if (*it == '"') {
        quoted_string(it, end, cxt);
    }
    else {
        unquoted_string(it, end, cxt);
    }

    cxt.emit_end_argument();
}

template<content_handler T>
void
skip_space(iterator& it, iterator const end, context<T>&)
{
    for (; it != end; ++it) {
        if (!is_space(*it)) break;
    }
}

template<content_handler T>
bool
scan(string_view const text, T& h)
{
    // assume text contains <any ASCII char> - <ASCII control char except CR LF TAB>
    auto it = text.begin(), end = text.end();
    context<T> cxt { h };

    try {
        while (it != end) {
            skip_space(it, end, cxt);
            if (it == end) break;

            argument(it, end, cxt);
        }
    }
    catch (stop const&) {
        return false;
    }

    return true;
}

std::error_category const& error_category();

inline std::error_code
make_error_code(errc const e)
{
    return { static_cast<int>(e), error_category() };
}

} // namespace stream9::xdg::scanner::exec_string

namespace std {

template<>
struct is_error_code_enum<stream9::xdg::scanner::exec_string::errc>
    : true_type {};

} // namespace std

#endif // STREAM9_DESKTOP_SCANNER_EXEC_STRING_HPP
