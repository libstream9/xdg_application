#ifndef STREAM9_DESKTOP_ENVIRONMENT_HPP
#define STREAM9_DESKTOP_ENVIRONMENT_HPP

#include <stream9/cstring_ptr.hpp>
#include <stream9/linux/directory.hpp>
#include <stream9/optional.hpp>
#include <stream9/outcome.hpp>
#include <stream9/string.hpp>

#include <sys/stat.h>
#include <time.h>

namespace stream9::xdg {

class environment
{
public:
    virtual ~environment() noexcept = default;

    virtual bool file_exists(cstring_ptr path) const noexcept;

    virtual bool directory_exists(cstring_ptr path) const noexcept;

    virtual string load_file(cstring_ptr path) const;

    virtual ::timespec save_file(cstring_ptr path, string_view content) const;

    virtual opt<struct ::stat> file_status(cstring_ptr dirpath) const;
    virtual opt<struct ::stat> file_status(lx::directory const&, string_view fname) const;
};

environment& env();

} // namespace stream9::xdg

#endif // STREAM9_DESKTOP_ENVIRONMENT_HPP
