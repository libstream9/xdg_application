#include <stream9/xdg/environment.hpp>

#include <stream9/filesystem/exists.hpp>
#include <stream9/filesystem/load_string.hpp>
#include <stream9/filesystem/temporary_file.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/rename.hpp>
#include <stream9/linux/stat.hpp>
#include <stream9/linux/write_n.hpp>
#include <stream9/path/dirname.hpp>

namespace stream9::xdg {

bool environment::
file_exists(cstring_ptr path) const noexcept
{
    try {
        return fs::exists(path);
    }
    catch (...) {
        return false;
    }
}

bool environment::
directory_exists(cstring_ptr path) const noexcept
{
    auto o_st = lx::nothrow::stat(path);
    if (o_st) {
        return lx::is_directory(*o_st);
    }
    else {
        return false;
    }
}

string environment::
load_file(cstring_ptr path) const
{
    try {
        return fs::load_string(path);
    }
    catch (...) {
        rethrow_error();
    }
}

::timespec environment::
save_file(cstring_ptr path, string_view content) const
{
    try {
        string dir { path::dirname(path) };

        fs::temporary_file f { dir | "/" };
        auto&& fd = f.fd();
        auto&& tmppath = f.path();

        lx::write_n(fd, content);

        lx::rename(tmppath, path);

        auto st = lx::fstat(fd);

        f.set_auto_delete(false);

        return st.st_mtim;
    }
    catch (...) {
        rethrow_error({
            { "path", path },
            { "content", content },
        });
    }
}

opt<struct ::stat> environment::
file_status(cstring_ptr path) const
{
    auto o_st = lx::nothrow::stat(path);
    if (o_st) {
        return *o_st;
    }
    else if (o_st.error() == lx::errc::enoent) {
        return null;
    }
    else {
        throw error {
            "stat(2)", o_st.error(), {
                { "path", path }
            }
        };
    }
}

opt<struct ::stat> environment::
file_status(lx::directory const& d, string_view n) const
{
    auto o_st = lx::nothrow::stat(d.fd(), n);
    if (o_st) {
        return *o_st;
    }
    else if (o_st.error() == lx::errc::enoent) {
        return null;
    }
    else {
        throw error {
            "stat(2)", o_st.error(), {
                { "dirpath", d.path() },
                { "fname", n },
            }
        };
    }
}

environment&
env()
{
    static environment instance;
    return instance;
}

} // namespace stream9::xdg
