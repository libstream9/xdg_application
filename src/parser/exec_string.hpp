#ifndef STREAM9_DESKTOP_PARSER_EXEC_STRING_HPP
#define STREAM9_DESKTOP_PARSER_EXEC_STRING_HPP

#include "../namespace.hpp"
#include "../parser.hpp"

#include <stream9/array.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace stream9::xdg::parser::exec_string {

array<string, 5>
    parse(string_view exec, location_tracker const& = {});

} // namespace stream9::xdg::parser::exec_string

#endif // STREAM9_DESKTOP_PARSER_EXEC_STRING_HPP
