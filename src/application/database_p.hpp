#ifndef STREAM9_XDG_APPLICATION_DATABASE_P_HPP
#define STREAM9_XDG_APPLICATION_DATABASE_P_HPP

#include <stream9/xdg/application/database.hpp>

#include <stream9/ref.hpp>
#include <stream9/sorted_table.hpp>
#include <stream9/unique_table.hpp>

namespace stream9::xdg::mime { class database; }

namespace stream9::xdg::applications::inline v2 {

class database::impl
{
public:
    // essentials
    impl(xdg::mime::database const&);

    ~impl() noexcept = default;

    impl(impl const&) = delete;
    impl& operator=(impl const&) = delete;
    impl(impl&&) noexcept = default;
    impl& operator=(impl&&) noexcept = default;

    // accessor
    auto& mime_db() const { return m_mime_db; }
    auto& id_to_fpath() { return m_id_to_fpath; }

    // query
    array<desktop_entry>
        find_applications(string_view mime_type) const;

    opt<desktop_entry>
        find_default_application(string_view mime_type) const;

    opt<desktop_entry>
        load_desktop_entry(string_view id) const noexcept;

    array<desktop_entry>
        all_applications() const;

    unique_array<string/*mime_type*/>
        associated_mime_types(string_view mime_type_prefix) const;

    // modifiers
    void refresh();

private:
    ref<xdg::mime::database const> m_mime_db;
    unique_table<string, string> m_id_to_fpath;
    sorted_table<string, string> m_mime_to_ids;
};

array<string> data_directories();
array<string> config_directories();

} // namespace stream9::xdg::applications::inline v2

#endif // STREAM9_XDG_APPLICATION_DATABASE_P_HPP
