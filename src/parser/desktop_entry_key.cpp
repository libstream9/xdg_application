#include "desktop_entry_key.hpp"

#include "../parser.hpp"
#include "../scanner/desktop_entry_key.hpp"

namespace stream9::xdg::parser::desktop_entry_key {

class parser
{
    using next_action = scanner::next_action;

public:
    parser(location_tracker const* locs,
           array<error>* errors) noexcept
        : m_locs { locs }
        , m_errors { errors }
    {}

    result run(string_view text)
    {
        try {
            m_text = text;

            scanner::desktop_entry_key::scan(text, *this);

            if (m_locale) {
                return {
                    *m_name,
                    { *m_locale, *m_lang, m_country, m_encoding, m_modifier },
                };
            }
            else {
                return { *m_name, {} };
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    void on_name(string_view s, next_action&) noexcept
    {
        m_name = s;
    }

    void on_locale(string_view s, next_action&) noexcept
    {
        m_locale = s;
    }

    void on_lang(string_view s, next_action&) noexcept
    {
        m_lang = s;
    }

    void on_country(string_view s, next_action&) noexcept
    {
        m_country = s;
    }

    void on_encoding(string_view s, next_action&) noexcept
    {
        m_encoding = s;
    }

    void on_modifier(string_view s, next_action&) noexcept
    {
        m_modifier = s;
    }

    void on_error(std::error_code const& e,
                  string_view s, next_action&)
    {
        auto loc = get_location(s);
        throw error {
            "parse error", e, {
                { "line", loc.line },
                { "column", loc.col },
            }
        };
    }

private:
    location get_location(string_view s) const
    {
        try {
            if (s.data() == nullptr) return {};

            if (m_locs) {
                return m_locs->get_location(s.begin());
            }
            else {
                return { 1, s.begin() - m_text->begin() };
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

private:
    opt<string_view> m_text;
    opt<string_view> m_name;
    opt<string_view> m_locale;
    opt<string_view> m_lang;
    opt<string_view> m_country;
    opt<string_view> m_encoding;
    opt<string_view> m_modifier;

    location_tracker const* m_locs = nullptr;
    array<error>* m_errors = nullptr;
};

result
parse(string_view text,
      location_tracker const* locs/*= nullptr*/,
      array<error>* errors/*= nullptr*/)
{
    try {
        if (locs) {
            parser p { locs, errors };

            return p.run(text);
        }
        else {
            location_tracker new_locs;
            new_locs.new_line(text.begin());

            parser p { &new_locs, errors };

            return p.run(text);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::parser::desktop_entry_key
