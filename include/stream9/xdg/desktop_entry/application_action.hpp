#ifndef STREAM9_XDG_APPLICATION_APPLICATION_ACTION_HPP
#define STREAM9_XDG_APPLICATION_APPLICATION_ACTION_HPP

#include "../error.hpp"

#include "key.hpp"
#include "locale.hpp"

#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace stream9::xdg::desktop_entry_ {

class entry_group;

class application_action
{
public:
    application_action(entry_group const&) noexcept;

    // query
    string_view key_name() const noexcept;

    string name(locale const& = {}) const;

    string icon(locale const& = {}) const;

    string exec() const;

    bool contains(key const&) const noexcept;

    string value(key const&) const;

private:
    entry_group const* m_group; // non-null
};

} // namespace stream9::xdg::desktop_entry_

#endif // STREAM9_XDG_APPLICATION_APPLICATION_ACTION_HPP
