#ifndef STREAM9_XDG_DESKTOP_ENTRY_ESCAPE_SEQUENCE_HPP
#define STREAM9_XDG_DESKTOP_ENTRY_ESCAPE_SEQUENCE_HPP

#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace stream9::xdg {

inline string
unescape(string_view v)
{
    try {
        string result;
        auto it = v.begin(), end = v.end();

        for (; it != end; ++it) {
            if (*it == '\\') [[unlikely]] {
                ++it;
                if (it == end) [[unlikely]] {
                    result.append('\\');
                    break;
                }
                else {
                    switch (*it) {
                        case 's':
                            result.append(' ');
                            break;
                        case 'n':
                            result.append('\n');
                            break;
                        case 't':
                            result.append('\t');
                            break;
                        case 'r':
                            result.append('\r');
                            break;
                        case '\\':
                            result.append('\\');
                            break;
                        [[unlikely]] default:
                            result.append('\\');
                            result.append(*it);
                            break;
                    }
                }
            }
            else {
                result.append(*it);
            }
        }

        return result;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg

#endif // STREAM9_XDG_DESKTOP_ENTRY_ESCAPE_SEQUENCE_HPP
