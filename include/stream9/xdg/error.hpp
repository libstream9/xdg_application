#ifndef STREAM9_DESKTOP_ERROR_HPP
#define STREAM9_DESKTOP_ERROR_HPP

#include <system_error>

#include <stream9/errors.hpp>

namespace stream9::xdg {

enum class errc {
    ok = 0,
    parse_error,
    association_error,
};

std::error_category const& error_category();

inline std::error_code
make_error_code(errc ec) noexcept
{
    return { static_cast<int>(ec), error_category() };
}

} // namespace stream9::xdg

namespace std {

template<>
struct is_error_code_enum<stream9::xdg::errc>
    : true_type {};

} // namespace std

#endif // STREAM9_DESKTOP_ERROR_HPP
