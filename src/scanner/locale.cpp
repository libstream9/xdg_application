#include "locale.hpp"

namespace stream9::xdg::scanner::locale {

class error_category_impl : public std::error_category
{
public:
    char const* name() const noexcept override
    {
        return "scanner::locale";
    }

    std::string message(int ev) const override
    {
        switch (static_cast<errc>(ev)) {
            case errc::ok:
                return "ok";
            case errc::invalid_char:
                return "invalid character";
            case errc::empty_lang:
                return "empty lang";
            case errc::empty_country:
                return "empty country";
            case errc::empty_encoding:
                return "empty encoding";
            case errc::empty_modifier:
                return "empty modifier";
            default:
                return "unknown error";
        }
    }
};

std::error_category const&
error_category() noexcept
{
    static error_category_impl impl;
    return impl;
}

} // namespace stream9::xdg::scanner::locale
