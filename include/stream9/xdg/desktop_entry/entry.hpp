#ifndef STREAM9_XDG_APPLICATION_ENTRY_HPP
#define STREAM9_XDG_APPLICATION_ENTRY_HPP

#include "key.hpp"

#include "../namespace.hpp"

#include <stream9/less.hpp>
#include <stream9/projection.hpp>
#include <stream9/string_view.hpp>
#include <stream9/unique_array.hpp>

#include <iosfwd>

namespace stream9::xdg::desktop_entry_ {

struct entry
{
    class key key;
    string_view value;

    bool operator==(entry const&) const = default;

    friend std::ostream& operator<<(std::ostream&, entry const&);
};

class entry_group
{
public:
    using entry_set = unique_array<entry, less, project<&entry::key>>;
    using iterator = entry_set::iterator;
    using const_iterator = entry_set::const_iterator;

public:
    entry_group(string_view name, entry_set&& entries) noexcept
        : m_name { name }
        , m_entries { std::move(entries) }
    {}

    // accessor
    string_view name() const noexcept { return m_name; }

    auto begin() const noexcept { return m_entries.begin(); }
    auto begin() noexcept { return m_entries.begin(); }
    auto end() const noexcept { return m_entries.end(); }
    auto end() noexcept { return m_entries.end(); }

    // query
    auto size() const noexcept { return m_entries.size(); }

    const_iterator find(key const&) const noexcept;

    bool contains(key const&) const noexcept;

    // modifier
    iterator erase(const_iterator from, const_iterator to) noexcept
    {
        return m_entries.erase(from, to);
    }

    // operator
    bool operator==(entry_group const&) const = default;

    friend std::ostream& operator<<(std::ostream&, entry_group const&);

private:
    string_view m_name;
    entry_set m_entries;
};

} // namespace stream9::xdg::desktop_entry_

#endif // STREAM9_XDG_APPLICATION_ENTRY_HPP
