#include "mime_info_cache.hpp"

#include "../parser.hpp"

#include <stream9/array.hpp>
#include <stream9/push_back.hpp>
#include <stream9/xdg/settings/scanner.hpp>

#include "../namespace.hpp"

namespace stream9::xdg::parser::mime_info_cache {

class parser
{
public:
    using next_action = settings::next_action;

public:
    void run(string_view text,
             mime_desktop_file_id_map& entries,
             array<error>* errors)
    {
        try {
            m_entries = &entries;
            m_errors = errors;

            m_locs = {};
            m_locs.new_line(text.begin());

            settings::scan(text, *this);
        }
        catch (...) {
            rethrow_error();
        }
    }

    void on_group_header(string_view s, next_action& a)
    {
        try {
            if (s == "MIME Cache") {
                if (m_got_header) {
                    push_error(errc::duplicated_group, s);
                    a = next_action::skip_group;
                }
                else {
                    m_got_header = true;
                }
            }
            else {
                push_error(errc::invalid_group_name, s);
                a = next_action::skip_group;
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    void on_entry(string_view mime_type,
                  string_view ids,
                  next_action& a)
    {
        try {
            if (!m_got_header) {
                push_error(errc::missing_required_group, mime_type);
                a = next_action::skip_group;
            }
            else {
                auto [_, ok] = m_entries->emplace(mime_type, ids);
                if (!ok) {
                    push_error(errc::duplicated_key, mime_type);
                }
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    void on_comment(string_view, next_action&)
    {}

    void on_error(std::error_code const& ec,
                  string_view range,
                  next_action&)
    {
        try {
            push_error(ec, range);
        }
        catch (...) {
            rethrow_error();
        }
    }

    void on_new_line(string_view::iterator it)
    {
        try {
            m_locs.new_line(it + 1);
        }
        catch (...) {
            rethrow_error();
        }
    }

private:
    void push_error(std::error_code const& ec,
                    string_view range,
                    opt<string_view> o_extra_info = {},
                    std::source_location where = std::source_location::current())
    {
        try {
            if (!m_errors) return;

            auto loc = m_locs.get_location(range.begin());

            json::object cxt {
                { "line", loc.line },
                { "column", loc.col },
                { "text", range },
            };
            if (o_extra_info) {
                cxt["detail"] = *o_extra_info;
            }

            st9::push_back(*m_errors,
                error { "parse error", ec, std::move(cxt),
                    std::move(where)
                });
        }
        catch (...) {
            rethrow_error();
        }
    }

    void push_error(errc e,
                    string_view range,
                    opt<string_view> o_extra_info = {},
                    std::source_location where = std::source_location::current())
    {
        push_error(make_error_code(e), range, o_extra_info, std::move(where));
    }

private:
    bool m_got_header = false;
    mime_desktop_file_id_map* m_entries;
    array<error>* m_errors;
    location_tracker m_locs;
};

std::error_category const&
error_category()
{
    struct impl : std::error_category {
        char const* name() const noexcept override
        {
            return "parser::mime_info_cache";
        }

        std::string message(int const e) const override
        {
            switch (static_cast<errc>(e)) {
                case errc::ok:
                    return "ok";
                case errc::missing_required_group:
                    return "missing required group";
                case errc::invalid_group_name:
                    return "invalid group name";
                case errc::duplicated_group:
                    return "duplicated group";
                case errc::duplicated_key:
                    return "duplicated key";
            }

            return "unknown error";
        }
    };

    static impl instance;

    return instance;
}

void parse(string_view text,
           mime_desktop_file_id_map& entries,
           array<error>* errors/*= nullptr*/)
{
    try {
        parser p;
        p.run(text, entries, errors);
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::xdg::parser::mime_info_cache
