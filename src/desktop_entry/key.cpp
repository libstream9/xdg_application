#include <stream9/xdg/desktop_entry/key.hpp>

#include "parser/desktop_entry_key.hpp"

#include <ostream>

namespace stream9::xdg::desktop_entry_ {

key::
key(string_view text)
    : m_name { "" }
{
    try {
        auto const& [ n, l ] = parser::desktop_entry_key::parse(text);
        m_name = n;
        m_locale = l;
    }
    catch (...) {
        throw_error(errc::parse_error);
    }
}

key::
key(string_view name, class locale const& loc)
    : m_name { "" }
{
    try {
        auto const& [ n, l ] = parser::desktop_entry_key::parse(name);

        m_name = n;
        m_locale = loc.empty() ? l : loc;
    }
    catch (...) {
        throw_error(errc::parse_error);
    }
}

std::ostream&
operator<<(std::ostream& os, key const& k)
{
    os << k.m_name;
    if (!k.m_locale.empty()) {
        os << '[' << k.m_locale << ']';
    }

    return os;
}

} // namespace stream9::xdg::desktop_entry_
