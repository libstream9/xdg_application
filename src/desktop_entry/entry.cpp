#include <stream9/xdg/desktop_entry/entry.hpp>

#include "namespace.hpp"

#include <stream9/strings/stream.hpp>

namespace stream9::xdg::desktop_entry_ {

static bool
match_locale(locale const& loc,
             string_view country,
             string_view modifier) noexcept
{
    return loc.country() == country
        && loc.modifier() == modifier;
}

static entry_group::const_iterator
locale_match(std::ranges::subrange<entry_group::const_iterator> const entries,
             locale const& loc) noexcept
{
    using std::ranges::find_if;
    using std::ranges::equal_range;

    assert(!entries.empty());

    auto const& lang = loc.lang();
    auto const& country = loc.country();
    auto const& modifier = loc.modifier();

    assert(!lang.empty());

    auto r = equal_range(entries, lang, {},
        [](auto&& e) { return e.key.locale().lang(); });

    auto get_locale = [](auto&& e) { return e.key.locale(); };
    auto match_with_country_modifier =
        [&](auto&& l) {
            return match_locale(l, country, modifier);
        };
    auto match_with_country =
        [&](auto&& l) {
            return match_locale(l, country, "");
        };
    auto match_with_modifier =
        [&](auto&& l) {
            return match_locale(l, "", modifier);
        };
    auto match_with_lang =
        [&](auto&& l) {
            return match_locale(l, "", "");
        };

    if (!country.empty() && !modifier.empty()) {
        auto it = find_if(
            r, match_with_country_modifier, get_locale);

        if (it != r.end()) return it;

        it = find_if(r, match_with_country, get_locale);

        if (it != r.end()) return it;

        it = find_if(r, match_with_modifier, get_locale);

        if (it != r.end()) return it;
    }
    else if (!country.empty() && modifier.empty()) {
        auto it = find_if(r, match_with_country, get_locale);

        if (it != r.end()) return it;
    }
    else if (country.empty() && !modifier.empty()) {
        auto it = find_if(r, match_with_modifier, get_locale);

        if (it != r.end()) return it;
    }

    auto it = find_if(r, match_with_lang, get_locale);

    if (it != r.end()) return it;

    it = entries.begin();
    assert(!it->key.is_localized());

    return it;
}

//
// entry
//
std::ostream&
operator<<(std::ostream& os, entry const& e)
{
    return os << "{ " << e.key << ", " << e.value << " }";
}

//
// entry_group
//
entry_group::const_iterator entry_group::
find(key const& key) const noexcept
{
    using std::ranges::equal_range;

    if (!key.is_localized()) {
        return m_entries.find(key);
    }
    else {
        auto r = equal_range(
            m_entries, key.name(), {}, [](auto&& e) { return e.key.name(); } );

        if (r.empty()) {
            return m_entries.end();
        }
        else {
            return locale_match(r, key.locale());
        }
    }
}

bool entry_group::
contains(key const& key) const noexcept
{
    return find(key) != m_entries.end();
}

std::ostream&
operator<<(std::ostream& os, entry_group const& g)
{
    os << "{ \"name\": " << g.name() << ", \"entries\": { ";

    for (auto const& e: g) {
        os << e << ", ";
    }

    os << "}";

    return os;
}

} // namespace stream9::xdg::desktop_entry_
