#ifndef STREAM9_DESKTOP_SCANNER_DESKTOP_ENTRY_KEY_HPP
#define STREAM9_DESKTOP_SCANNER_DESKTOP_ENTRY_KEY_HPP

#include "../scanner.hpp"

#include "locale.hpp"

#include <stream9/safe_integer.hpp>
#include <stream9/string_view.hpp>

#include <system_error>

namespace stream9::xdg::scanner::desktop_entry_key {

// Grammar
//
// key := name ?('[' locale ']')
// name := identifier
// locale := lang ?('_' country) ?('.' encoding) ?('@' modifier)
// lang := identifier
// country := identifier
// encoding := identifier
// modifier := identifier
// identifier := +[A-Za-z0-9-]

template<typename T>
concept content_handler =
    requires (T&& t, string_view s,
              std::error_code e, next_action& a)
    {
        t.on_name(s, a);
        t.on_lang(s, a);
        t.on_country(s, a);
        t.on_encoding(s, a);
        t.on_modifier(s, a);
        t.on_locale(s, a);
        t.on_error(e, s, a);
    };

template<content_handler T>
bool scan(string_view text, T&);

enum class errc {
    ok = 0,
    invalid_char,
    empty_name,
    terminator_is_expected,
    end_of_string_is_expected,
};

// Implementation

template<content_handler T>
struct context
{
    T& handler;

    context(T& h) : handler { h } {}
};

std::error_category const& error_category();

inline std::error_code
make_error_code(errc const c)
{
    return std::error_code(
        static_cast<int>(c),
        error_category()
    );
}

using iterator = string_view::iterator;

template<content_handler T>
void
emit_error(context<T>& cxt, errc e, iterator it)
{
    next_action a = next_action::stop;
    cxt.handler.on_error(make_error_code(e), { it, it + 1 }, a);
    execute_action(a);
}

template<content_handler T>
void
emit_error(context<T>& cxt, errc e, string_view s)
{
    next_action a = next_action::stop;
    cxt.handler.on_error(make_error_code(e), s, a);
    execute_action(a);
}

template<content_handler T>
void
emit_name(context<T>& cxt, string_view s)
{
    next_action a = next_action::proceed;
    cxt.handler.on_name(s, a);
    execute_action(a);
}

static bool
is_identifier_char(auto c)
{
    return ('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z')
        || ('0' <= c && c <= '9') || c == '-';
}

template<content_handler T>
void
locale(iterator& it, iterator end, context<T>& cxt)
{
    auto is_end = [](auto it, auto end) {
        return it == end || *it == ']';
    };
    if (!scanner::locale::scan(it, end, cxt.handler, is_end)) {
        throw stop();
    }
}

template<content_handler T>
void
name(iterator& it, iterator end, context<T>& cxt)
{
    if (it == end) {
        emit_error(cxt, errc::empty_name, { it, end });
    }

    auto save = it;

    for (; it != end; ++it) {
        if (*it == '[') break;
        if (!is_identifier_char(*it)) {
            emit_error(cxt, errc::invalid_char, it);
        }
    }

    if (save == it) {
        emit_error(cxt, errc::empty_name, { save, it });
    }

    emit_name(cxt, { save, it });
}

template<content_handler T>
void
key(iterator& it, iterator end, context<T>& cxt)
{
    name(it, end, cxt);

    if (it != end && *it == '[') {
        ++it;

        locale(it, end, cxt);

        if (it == end || *it != ']') {
            auto it2 = it == end ? end : it + 1;
            emit_error(cxt, errc::terminator_is_expected, { it, it2 });
        }
        else {
            ++it;
        }

    }

    if (it != end) {
        emit_error(cxt, errc::end_of_string_is_expected, { it, end });
    }
}

template<content_handler T>
bool
scan(string_view text, T& h)
{
    try {
        context<T> cxt { h };

        auto it = text.begin();

        key(it, text.end(), cxt);
    }
    catch (stop const&) {
        return false;
    }

    return true;
}

} // namespace stream9::xdg::scanner::desktop_entry_key

namespace std {

template<>
struct is_error_code_enum<stream9::xdg::scanner::desktop_entry_key::errc>
    : true_type {};

} // namespace std

#endif // STREAM9_DESKTOP_SCANNER_DESKTOP_ENTRY_KEY_HPP
