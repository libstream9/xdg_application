#include "exec_string.hpp"

namespace stream9::xdg::scanner::exec_string {

struct error_category_impl : std::error_category {
    char const* name() const noexcept override
    {
        return "stream9::xdg::scanner::exec_string";
    }

    std::string message(int const ev) const override
    {
        switch (static_cast<errc>(ev)) {
            case errc::ok:
                return "ok";
            case errc::unclosed_quote:
                return "unclosed quote";
            case errc::unquoted_reserved_char:
                return "unquoted reserved char";
            case errc::unescaped_char:
                return "unescaped char";
            case errc::unknown_escape_sequence:
                return "unknown escape sequence";
        }

        return "unknown error";
    }
};

std::error_category const&
error_category()
{
    static error_category_impl instance;

    return instance;
}

} // namespace stream9::xdg::scanner::exec_string
